

function testImpl(v, id, name, ec) {

    var Future = Java.type("demo.JsFuture");

    v.print("from console");
    var User = Java.type("demo.Nashorn.User");
    v.drink("hujasse");
    var fut = Future.adapt(v.futureTest());
    var res1 = fut.map(ec, function (res) {
        var result = v.log(res);
        v.print(res)
        return "fut1 completed";
    });
    var fut2 = Future.adapt(v.future2());
    var fut3 = res1.zip(fut2).map(ec, function(x) {
        v.print(x._1);
        v.print(x._2);
        return x;
    });
    return fut3.toScala();
}


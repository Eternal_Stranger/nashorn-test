package demo

import demo.Nashorn.User

import scala.concurrent.{ExecutionContext, Future}


/**
  * Created by moroz on 9/27/2016.
  */
trait TestApi {
  def testImpl(v: Vodka, id: String, name: String, ec: ExecutionContext): Future[Any]
}

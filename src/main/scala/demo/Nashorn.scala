package demo

import java.util.concurrent.Executors
import javax.script._

import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.{ExecutionContext, Future}



object Nashorn extends App with LazyLogging {

  private def toAlphabetic(n: Int): String = {
    val i = n - 1
    if(i < 0) {
      "-"
    } else {
      val quot = i / 26
      val rem = i % 26
      val letter = ('A'.toInt + rem).toChar
      if( quot == 0 ) {
        "" + letter
      } else {
        toAlphabetic(quot-1) + letter
      }
    }
  }



  case class User(id: Long, name: String)

  override def main(args: Array[String]) = {

    val ggg = toAlphabetic(27)
    logger.info(ggg)

    implicit val ec = new ExecutionContext {
      val threadPool = Executors.newFixedThreadPool(1000)

      def execute(runnable: Runnable) {
        threadPool.submit(runnable)
      }

      def reportFailure(t: Throwable) {}
    }

    val ENGINE_NAME = "nashorn"
    val engine = new ScriptEngineManager()
      .getEngineByName(ENGINE_NAME)
      .asInstanceOf[ScriptEngine with Invocable with Compilable]


    val f = loadScript(engine)

    val v = new Vodka(ec)
    val executor = engine.getInterface(classOf[TestApi])
    val res = executor.testImpl(v, "324", "345", ec)

    val g = res.map { x =>
      logger.info("js result from scala: " + x.toString)
    }
    logger.info(g.toString)
  }

  def loadScript(engine: ScriptEngine) = {
    engine.eval("load('src/main/js/test.js')")
  }
}

class Vodka(ec: ExecutionContext) extends LazyLogging {

  def print(data: String) = logger.info("print: " + data)

  def drink(doze: String) = {
    logger.info(doze)
  }

  def futureTest() = {
    scala.concurrent.Future("future result ok")(ec)
  }

  def log(text: String) = {
    logger.info("log: " + text)
    "return from future"
  }

  def future2() = {
    scala.concurrent.Future {
      Thread.sleep(500)
      logger.info("fut 2 completeed")
      "future 2 result ok"
    }(ec)
  }
}

package demo

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}
import java.util.function
import java.util.function.BiFunction

object JsFuture {

  def failed(t: Throwable): JsFuture[_] = new JsFuture(scala.concurrent.Future.failed(t))

  def adapt[A](future: scala.concurrent.Future[A]): JsFuture[A] = new JsFuture(future)

}

class JsFuture[A](private val future: scala.concurrent.Future[A]) {

  def toScala = future

  def scalaMap[U](f: A => U)(implicit ec: ExecutionContext): Future[U] = {
    future.map(x => f(x))(ec)
  }

  def map[U](executor: ExecutionContext, f: java.util.function.Function[A, U]): JsFuture[U] =
    new JsFuture(future.map(x => f(x))(executor))

  def flatMap[U](executor: ExecutionContext, f: java.util.function.Function[A, JsFuture[U]]): JsFuture[U] =
    new JsFuture(future.flatMap(x => f(x).future)(executor))

  def zip[U](that: JsFuture[U]): JsFuture[(A, U)] = new JsFuture(this.future.zip(that.future))

  def onComplete(executor: ExecutionContext, f: java.util.function.Function[Try[A], _]): Unit =
    future.onComplete(x => f(x))(executor)

  def onComplete2(executor: ExecutionContext, s: java.util.function.Function[A, _], f: java.util.function.Function[Throwable, _]): Unit = {
    future.onComplete {
      case Success(x) => s(x)
      case Failure(t) => f(t)
    }(executor)
  }

}